# yang2db-xsl
XSLT 1.0 templates for translating NETCONF YANG (via yin XML) to DocBook content.

Example use:
Translate YANG file to yin XML with pyang (https://github.com/mbj4668/pyang)

pyang -f yin yangfile.yang -o yangfile.yin
xsltproc -o yangfile.xml yin2db.xsl yangfile.yin
