<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:yin="urn:ietf:params:xml:ns:yang:yin:1"
    xmlns:xml="http://www.w3.org/XML/1998/namespace"
    xmlns:tailf="http://tail-f.com/yang/common"
    xmlns="http://docbook.org/ns/docbook"
    version="1.0">
  <xsl:output indent="yes" omit-xml-declaration="no" encoding="UTF-8"/>
  <xsl:preserve-space elements="para emphasis"/>

  <xsl:template match="yin:module/yin:organization|yin:module/yin:contact|yin:module/yin:revision|yin:module/yin:import"/>

  <xsl:template match="yin:module">
    <article>
      <title>YANG module definitions</title>
      <section>
        <xsl:attribute name="xml:id">config_module_<xsl:value-of select="./@name"/></xsl:attribute>
        <title><xsl:value-of select="./@name"/></title>
        <xsl:apply-templates/>
      </section>
    </article>
  </xsl:template>

  <xsl:template match="yin:description">
    <para>
      <xsl:value-of select="./yin:text"/>
    </para>
  </xsl:template>
  
  <xsl:template match="yin:feature">
    <section>
      <xsl:attribute name="xml:id">feature_<xsl:value-of select="./@name"/></xsl:attribute>
      <title>Feature: <xsl:value-of select="./@name"/></title>
      <xsl:apply-templates/>
    </section>
  </xsl:template>
  
  <xsl:template match="yin:module/yin:identity">
    <section>
      <title>Identity: <xsl:value-of select="./@name"/></title>
      <xsl:apply-templates/>
    </section>
  </xsl:template>
  <xsl:template match="yin:module/yin:typedef">
    <section>
      <title>Type definition: <xsl:value-of select="./@name"/></title>
      <xsl:apply-templates/>
    </section>
  </xsl:template>

  <xsl:template match="yin:grouping">
    <section>
      <xsl:attribute name="xml:id">grouping_<xsl:value-of select="./@name"/></xsl:attribute>
      <title>Grouping: <xsl:value-of select="./@name"/></title>
      <xsl:apply-templates/>
    </section>
  </xsl:template>

  <xsl:template match="yin:choice">
    <itemizedlist>
      <title>Choice <literal><xsl:value-of select="./@name"/></literal></title>
      <para>One of:</para>
        <xsl:for-each select="yin:case">
          <listitem>
            <para>
              <literal><xsl:value-of select="./@name"/></literal>
            </para>
            <xsl:apply-templates/>
          </listitem>
        </xsl:for-each>
    </itemizedlist>
  </xsl:template>

  <xsl:template match="yin:if-feature">
    <para>Depends on
    <xref>
      <xsl:attribute name="linkend">feature_<xsl:value-of select="./@name"/></xsl:attribute>
    </xref>
    </para>
  </xsl:template>
  
  <xsl:template match="yin:uses">
    <para>Uses 
    <xref>
      <xsl:attribute name="linkend">grouping_<xsl:value-of select="./@name"/>
      </xsl:attribute>
    </xref>
    </para>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="yin:case/yin:container|yin:list/yin:container">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="yin:module/yin:container">
    <section>
      <title>Container <literal><xsl:value-of select="./@name"/></literal></title>
      <xsl:apply-templates/>
    </section>
  </xsl:template>
  
  <xsl:template match="yin:list">
    <para>List <literal><xsl:value-of select="@name"/></literal></para>
    <para>List of entries, key <literal><xsl:value-of select="yin:key/@value"/></literal></para>
    <itemizedlist>
      <xsl:for-each select="yin:leaf|yin:container">
        <listitem>
          <xsl:apply-templates/>
        </listitem>
      </xsl:for-each>
    </itemizedlist>
  </xsl:template>

  <xsl:template match="yin:leaf|yin:leaf-list">
    <xsl:variable name="typename" select="yin:type/@name"/>
    <para><literal><xsl:value-of select="@name"/></literal></para>
    <xsl:if test="yin:description">
      <xsl:apply-templates select="yin:description"/>
    </xsl:if>
    <xsl:if test="yin:default">
      <para>
        Default
        <xsl:value-of select="yin:default/@value"/>
      </para>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
